output "openstack_networking_secgroup" {
  value       = openstack_networking_secgroup_v2.cluster_net.name
  description = "Name of the server group for the project with its environment"
}
output "openstack_networking_network" {
  value       = openstack_networking_network_v2.network_1.name
  description = "Name of the cluster network for the project with its environment"
}
output "openstack_networking_subnet" {
  value       = openstack_networking_subnet_v2.subnet_1.id
  description = "Name of the subtnet project with its environment"
}
output "openstack_networking_secgroup_rule" {
  value       = openstack_networking_secgroup_rule_v2.allow_higher_ports.remote_ip_prefix
  description = "The remote CIDR, the value needs to be a valid CIDR (i.e. 192.168.0.0/16). Changing this creates a new security group rule."
}
output "openstack_compute_servergroup" {
  value       = openstack_compute_servergroup_v2.anti_affinity.id
  description = "Id for anti-affinity rules for the server group"
}
output "openstack_compute_image" {
  value       = data.openstack_images_image_v2.image.name
  description = "Name for the open stack image "
}

