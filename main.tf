locals {
  server_group_name    = "server-group-${var.project}-${var.environment}"
  cluster_network_name = "cluster-network-${var.project}-${var.environment}"
  subnet_name          = "subnet-${var.project}-${var.environment}"
  router_name          = "router-${var.project}-${var.environment}"
  anti_affinity        = "anti-affinity-${var.project}-${var.environment}"
}

resource "openstack_networking_secgroup_v2" "cluster_net" {
  name        = local.server_group_name
  description = "Don't let just anyone in"
}
# Add rules to internal networks security group.
resource "openstack_networking_secgroup_rule_v2" "allow_ipv4_within_group" {
  direction         = "ingress"
  ethertype         = "IPv4"
  remote_group_id   = openstack_networking_secgroup_v2.cluster_net.id
  security_group_id = openstack_networking_secgroup_v2.cluster_net.id
}
resource "openstack_networking_secgroup_rule_v2" "allow_ipv6_within_group" {
  direction         = "ingress"
  ethertype         = "IPv6"
  remote_group_id   = openstack_networking_secgroup_v2.cluster_net.id
  security_group_id = openstack_networking_secgroup_v2.cluster_net.id
}
resource "openstack_networking_secgroup_rule_v2" "allow_ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  port_range_min    = 22
  port_range_max    = 22
  protocol          = "tcp"
  security_group_id = openstack_networking_secgroup_v2.cluster_net.id
}
resource "openstack_networking_secgroup_rule_v2" "allow_icmp" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  security_group_id = openstack_networking_secgroup_v2.cluster_net.id
}
resource "openstack_networking_secgroup_rule_v2" "allow_icmp6" {
  direction         = "ingress"
  ethertype         = "IPv6"
  protocol          = "ipv6-icmp"
  security_group_id = openstack_networking_secgroup_v2.cluster_net.id
}
resource "openstack_networking_secgroup_rule_v2" "allow_higher_ports" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  security_group_id = openstack_networking_secgroup_v2.cluster_net.id
  port_range_min    = 30000
  port_range_max    = 32767
  remote_ip_prefix  = var.openstack_subnet_cidr
}


# Create cluster internal network.
resource "openstack_networking_network_v2" "network_1" {
  name           = local.cluster_network_name
  admin_state_up = true
}
resource "openstack_networking_subnet_v2" "subnet_1" {
  name        = local.subnet_name
  network_id  = openstack_networking_network_v2.network_1.id
  cidr        = var.openstack_subnet_cidr
  ip_version  = 4
  enable_dhcp = true
  allocation_pool {
    start = var.openstack_subnet_allocation_pool_start
    end   = var.openstack_subnet_allocation_pool_end
  }
  # Internal IPs of DNS Servers in the cloud.
  dns_nameservers = var.openstack_dns_nameserver
}

resource "openstack_compute_servergroup_v2" "anti_affinity" {
  name     = local.anti_affinity
  policies = ["anti-affinity"]
}

# Set up a router to allow access through public internet.
data "openstack_networking_network_v2" "external" {
  name = var.floating_ip_pool
}
resource "openstack_networking_router_v2" "router_1" {
  name                = local.router_name
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.external.id
}
resource "openstack_networking_router_interface_v2" "router_interface_1" {
  router_id = openstack_networking_router_v2.router_1.id
  subnet_id = openstack_networking_subnet_v2.subnet_1.id
}
data "openstack_images_image_v2" "image" {
  most_recent = true
  visibility  = "public"
  properties = {
    os_distro  = var.openstack_images_distro
    os_version = var.openstack_images_version
  }
}
