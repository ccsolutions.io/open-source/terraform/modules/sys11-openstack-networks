module "example" {
  source                             = "git:https://gitlab.com/ccsolutions.io/open-source/terraform/modules/sys11-openstack-networks.git?ref=v1.7.3"
  environment                        = var.environment
  project                            = var.project
  syseleven_credential_id            = var.syseleven_credential_id
  syseleven_credential_secret        = var.syseleven_credential_secret
  openstack_networking_secgroup      = var.openstack_networking_secgroup
  openstack_networking_network       = var.openstack_networking_network
  openstack_networking_subnet        = var.openstack_networking_subnet
  openstack_networking_secgroup_rule = var.openstack_networking_secgroup_rule
  openstack_compute_servergroup      = var.openstack_compute_servergroup
  openstack_images_distro            = var.openstack_images_distro
  openstack_images_version           = var.openstack_images_version
}
