## Syseleven Openstack Network

<!-- BEGINNING OF PRE-COMMIT-OPENTOFU DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | 1.7.3 |
| <a name="requirement_openstack"></a> [openstack](#requirement\_openstack) | 2.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_openstack"></a> [openstack](#provider\_openstack) | 2.0.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [openstack_compute_servergroup_v2.anti_affinity](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/2.0.0/docs/resources/compute_servergroup_v2) | resource |
| [openstack_networking_network_v2.network_1](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/2.0.0/docs/resources/networking_network_v2) | resource |
| [openstack_networking_router_interface_v2.router_interface_1](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/2.0.0/docs/resources/networking_router_interface_v2) | resource |
| [openstack_networking_router_v2.router_1](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/2.0.0/docs/resources/networking_router_v2) | resource |
| [openstack_networking_secgroup_rule_v2.allow_higher_ports](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/2.0.0/docs/resources/networking_secgroup_rule_v2) | resource |
| [openstack_networking_secgroup_rule_v2.allow_icmp](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/2.0.0/docs/resources/networking_secgroup_rule_v2) | resource |
| [openstack_networking_secgroup_rule_v2.allow_icmp6](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/2.0.0/docs/resources/networking_secgroup_rule_v2) | resource |
| [openstack_networking_secgroup_rule_v2.allow_ipv4_within_group](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/2.0.0/docs/resources/networking_secgroup_rule_v2) | resource |
| [openstack_networking_secgroup_rule_v2.allow_ipv6_within_group](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/2.0.0/docs/resources/networking_secgroup_rule_v2) | resource |
| [openstack_networking_secgroup_rule_v2.allow_ssh](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/2.0.0/docs/resources/networking_secgroup_rule_v2) | resource |
| [openstack_networking_secgroup_v2.cluster_net](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/2.0.0/docs/resources/networking_secgroup_v2) | resource |
| [openstack_networking_subnet_v2.subnet_1](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/2.0.0/docs/resources/networking_subnet_v2) | resource |
| [openstack_images_image_v2.image](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/2.0.0/docs/data-sources/images_image_v2) | data source |
| [openstack_networking_network_v2.external](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/2.0.0/docs/data-sources/networking_network_v2) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_environment"></a> [environment](#input\_environment) | (Required)The name of the Environment | `string` | n/a | yes |
| <a name="input_floating_ip_pool"></a> [floating\_ip\_pool](#input\_floating\_ip\_pool) | (Optional) The name of the network. Changing this updates the name of the existing network. | `string` | `"ext-net"` | no |
| <a name="input_openstack_dns_nameserver"></a> [openstack\_dns\_nameserver](#input\_openstack\_dns\_nameserver) | (Optional) An array of DNS name server names used by hosts in this subnet. Changing this updates the DNS name servers for the existing subnet. | `list(string)` | <pre>[<br>  "37.123.105.116",<br>  "37.123.105.117"<br>]</pre> | no |
| <a name="input_openstack_images_distro"></a> [openstack\_images\_distro](#input\_openstack\_images\_distro) | (Required) The name in the distro | `string` | `"ubuntu"` | no |
| <a name="input_openstack_images_version"></a> [openstack\_images\_version](#input\_openstack\_images\_version) | (Required) The version image in the distro | `string` | `"20.04"` | no |
| <a name="input_openstack_subnet_allocation_pool_end"></a> [openstack\_subnet\_allocation\_pool\_end](#input\_openstack\_subnet\_allocation\_pool\_end) | (Optional) OpenStack Network Range, last IP of the Pool | `string` | `"192.168.1.254"` | no |
| <a name="input_openstack_subnet_allocation_pool_start"></a> [openstack\_subnet\_allocation\_pool\_start](#input\_openstack\_subnet\_allocation\_pool\_start) | (Optional) OpenStack Network Range, first IP of the Pool | `string` | `"192.168.1.2"` | no |
| <a name="input_openstack_subnet_cidr"></a> [openstack\_subnet\_cidr](#input\_openstack\_subnet\_cidr) | (Optional) CIDR representing IP range for the version. You can omit this option if you are creating a subnet from a subnet pool. | `string` | `"192.168.1.0/24"` | no |
| <a name="input_project"></a> [project](#input\_project) | (Required)The name of the Project/Customer | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_openstack_compute_image"></a> [openstack\_compute\_image](#output\_openstack\_compute\_image) | Name for the open stack image |
| <a name="output_openstack_compute_servergroup"></a> [openstack\_compute\_servergroup](#output\_openstack\_compute\_servergroup) | Id for anti-affinity rules for the server group |
| <a name="output_openstack_networking_network"></a> [openstack\_networking\_network](#output\_openstack\_networking\_network) | Name of the cluster network for the project with its environment |
| <a name="output_openstack_networking_secgroup"></a> [openstack\_networking\_secgroup](#output\_openstack\_networking\_secgroup) | Name of the server group for the project with its environment |
| <a name="output_openstack_networking_secgroup_rule"></a> [openstack\_networking\_secgroup\_rule](#output\_openstack\_networking\_secgroup\_rule) | The remote CIDR, the value needs to be a valid CIDR (i.e. 192.168.0.0/16). Changing this creates a new security group rule. |
| <a name="output_openstack_networking_subnet"></a> [openstack\_networking\_subnet](#output\_openstack\_networking\_subnet) | Name of the subtnet project with its environment |
<!-- END OF PRE-COMMIT-OPENTOFU DOCS HOOK -->
