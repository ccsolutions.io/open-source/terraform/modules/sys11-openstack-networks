variable "environment" {
  description = "(Required)The name of the Environment"
  type        = string
}
variable "project" {
  description = "(Required)The name of the Project/Customer"
  type        = string
}
variable "openstack_subnet_cidr" {
  description = "(Optional) CIDR representing IP range for the version. You can omit this option if you are creating a subnet from a subnet pool."
  type        = string
  default     = "192.168.1.0/24"
}
variable "openstack_subnet_allocation_pool_start" {
  description = "(Optional) OpenStack Network Range, first IP of the Pool"
  type        = string
  default     = "192.168.1.2"
}
variable "openstack_subnet_allocation_pool_end" {
  description = "(Optional) OpenStack Network Range, last IP of the Pool"
  type        = string
  default     = "192.168.1.254"
}
variable "openstack_dns_nameserver" {
  description = "(Optional) An array of DNS name server names used by hosts in this subnet. Changing this updates the DNS name servers for the existing subnet."
  type        = list(string)
  default     = ["37.123.105.116", "37.123.105.117"]
}
variable "floating_ip_pool" {
  description = "(Optional) The name of the network. Changing this updates the name of the existing network."
  type        = string
  default     = "ext-net"
}
variable "openstack_images_distro" {
  description = "(Required) The name in the distro"
  type        = string
  default     = "ubuntu"
}
variable "openstack_images_version" {
  description = "(Required) The version image in the distro"
  type        = string
  default     = "20.04"
}

