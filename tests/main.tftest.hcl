variables {
  environment                            = "development"
  project                                = "captain-america"
  openstack_images_distro                = "ubuntu"
  openstack_images_version               = "20.04"
  openstack_subnet_cidr                  = "192.254.254.0/24"
  openstack_subnet_allocation_pool_start = "192.254.254.20"
  openstack_subnet_allocation_pool_end   = "192.254.254.254"
  openstack_dns_nameserver               = ["37.123.105.116", "8.8.8.8"]
}

provider "openstack" {

}

run "create_network" {
  command = apply

  assert {
    condition     = openstack_compute_servergroup_v2.anti_affinity.name == "anti-affinity-captain-america-development"
    error_message = "Server name is not matching with expected value"
  }

  assert {
    condition     = openstack_networking_network_v2.network_1.name == "cluster-network-captain-america-development"
    error_message = "The cluster network name is  not matching with expected value"
  }

  assert {
    condition     = openstack_networking_secgroup_rule_v2.allow_higher_ports.remote_ip_prefix == var.openstack_subnet_cidr
    error_message = "Remote prefix IP not matching with expected value"
  }

  assert {
    condition     = openstack_networking_secgroup_v2.cluster_net.name == "server-group-captain-america-development"
    error_message = "Remote prefix IP not matching with expected value"
  }

  assert {
    condition     = openstack_networking_secgroup_v2.cluster_net.name == "server-group-captain-america-development"
    error_message = "Remote prefix IP not matching with expected value"
  }

  assert {
    condition     = openstack_networking_subnet_v2.subnet_1.dns_nameservers[0] == var.openstack_dns_nameserver[0]
    error_message = "Remote prefix IP not matching with expected value"
  }
}
